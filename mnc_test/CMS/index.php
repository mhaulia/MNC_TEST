<?php
include "config/koneksi.php";

date_default_timezone_set("Asia/Jakarta");

$page=mysql_real_escape_string(htmlentities($_GET['page']));
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Administrator">
<meta name="author" content="Muhaulia">
<title>Administrator</title>

<link href="<?php echo $base_url;?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $base_url;?>css/datepicker3.css" rel="stylesheet">
<link href="<?php echo $base_url;?>css/styles.css" rel="stylesheet">
<link href="<?php echo $base_url;?>css/bootstrap-table.css" rel="stylesheet">

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Sisfo</span>Helpdesk</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> User <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><span class="glyphicon glyphicon-cog"></span> Ganti Password</a></li>
							<li><a href="#"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<ul class="nav menu">
			<li class="<?php if($page==""){ echo "active"; }?>"><a href="<?php echo $base_url;?>index"><span class="glyphicon glyphicon-home"></span> Beranda</a></li>			
			
			<li class="parent">
				<a href="#">
					<span class="glyphicon glyphicon-list-alt"></span> Master Data <span data-toggle="collapse" href="#nilai" class="icon pull-right"><em class="glyphicon glyphicon-s glyphicon-plus"></em></span> 
				</a>
				<ul class="children collapse in" id="nilai">			
					<li>
						<a class="" href="<?php echo $base_url;?>kategori">
							<span class="glyphicon glyphicon-share-alt"></span> Kategori Teknisi
						</a>
					</li>
					<li>
						<a class="" href="<?php echo $base_url;?>teknisi">
							<span class="glyphicon glyphicon-share-alt"></span> Teknisi
						</a>
					</li>					
				</ul>
			</li>
			
			<li role="presentation" class="divider"></li>
			<li class="parent">
				<a href="#">
					<span class="glyphicon glyphicon-list-alt"></span> Helpdesk Report <span data-toggle="collapse" href="#nilai" class="icon pull-right"><em class="glyphicon glyphicon-s glyphicon-plus"></em></span> 
				</a>
				<ul class="children collapse in" id="nilai">			
					<li>
						<a class="" href="<?php echo $base_url;?>request">
							<span class="glyphicon glyphicon-share-alt"></span> User Request
						</a>
					</li>
								
				</ul>
			</li>
			
			<li role="presentation" class="divider"></li>
			<li><a href="#"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
		</ul>
		<div class="attribution">&copy; <?php echo date("Y"); ?> by Muhammad Aulia Musthofa</div>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">					
		<?php
        $file='module/'.$page.'.php';
  
        if(empty($page)){
            include 'module/home.php';
        }
        elseif(!file_exists($file)){
            echo "<h1>404 ERROR!!!</h1>Page Not Found.";
        }
        else{
            include 'module/'.$page.'.php';
        }
        ?>
	</div>	<!--/.main-->

	<script src="<?php echo $base_url;?>js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo $base_url;?>js/bootstrap.min.js"></script>
	<script src="<?php echo $base_url;?>js/chart.min.js"></script>
	<script src="<?php echo $base_url;?>js/chart-data.js"></script>
	<script src="<?php echo $base_url;?>js/easypiechart.js"></script>
	<script src="<?php echo $base_url;?>js/easypiechart-data.js"></script>
	<script src="<?php echo $base_url;?>js/bootstrap-datepicker.js"></script>
	<script src="<?php echo $base_url;?>js/bootstrap-table.js"></script>
	<script>
		$('#calendar').datepicker({
		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>

</html>
